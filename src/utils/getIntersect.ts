
import { polygon } from "@turf/helpers"
import intersect from "@turf/intersect"

const toTurfPolygon = (rawPolygon) => {
  const paths = rawPolygon.paths.map((point) => [point.lat, point.lng])
  const toTransform = [[...paths, [paths[0][0],paths[0][1]]]]
 return polygon(toTransform)
}

const getIntersect = (newPolygon, polygon) =>
  intersect(toTurfPolygon(newPolygon), toTurfPolygon(polygon))

const getOverlapPolygons = (newPolygon, polygons) => {
  const overlapPolygons: any[] = [];
  // var overlapPolygonId = 1;
  polygons
    .filter((polygon) => polygon.districtid !== newPolygon.districtid)
    .forEach((polygon) => {
      const overlapPolygon = getIntersect(newPolygon, polygon);
      if (overlapPolygon !== null) {
        // overlapPolygon.geometry.coordinates
        overlapPolygons.push(overlapPolygon);
        // overlapPolygonId ++;
      }
    });
    return overlapPolygons;
}

export default getIntersect;
export { getOverlapPolygons };
