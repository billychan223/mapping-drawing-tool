const toCsv = (polygons) => {
    const csvContent = []
    csvContent.push(["DistrictId","Sequence","Latitude","Longitude"]);
    polygons.forEach(polygon => {
        const districtid = polygon.districtid
        var sequence = 1
        polygon.paths.forEach(path => {
            csvContent.push([districtid,sequence,path.lat,path.lng]);
            sequence++;
        })
    });
    return csvContent;
  };
  
  export default toCsv;
  