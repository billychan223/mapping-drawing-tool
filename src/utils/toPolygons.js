const toPolygons = (datas) => {
  const polygons = [];
  const newDatas = datas.sort(sortPolygons);
  const noOfPolygons = newDatas[newDatas.length - 1].districtid;
  for (var i = 1; i <= noOfPolygons; i++) {
    const currentID = i;
    const paths = newDatas
      .filter((data) => data.districtid === currentID)
      .map((data) => ({ lat: data.latitude, lng: data.longitude }));
    polygons.push({districtid: currentID, paths: paths});
  }
  return polygons;
};

const sortPolygons = (a, b) =>
a.districtid > b.districtid
  ? 1
  : a.districtid === b.districtid
  ? a.sequence > b.sequence
    ? 1
    : -1
  : -1;

export default toPolygons;
export { sortPolygons };
