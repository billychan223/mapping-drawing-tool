import React from "react";
import MyMap from "./MyMap";

const styles = {
  container: {
    width: '100%',
    height: '90vh'
  }
}

const App = () => {

  return (
    <div className="App">
      <MyMap styles={styles}/>
    </div>
  );
}

export default App;
