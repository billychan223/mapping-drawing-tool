import { GoogleMap, LoadScript } from "@react-google-maps/api";
import React, { useCallback, useRef } from "react";
import PropTypes from "prop-types";
import MyCsvReader from "./MyCsvReader";
import MyMapContent from "./MyMapContent";
import toCsv from "../utils/toCsv";

const center = { lat: 22.302711, lng: 114.177216 };

const libraries = ["drawing"];

const MyMapPropTypes = {
  styles: PropTypes.shape({
    container: PropTypes.object.isRequired,
  }).isRequired,
};

const MyMap = ({ styles }) => {
  const mapRef = useRef(null);
  const myMapContentRef = useRef(null);

  const onLoad = useCallback((map) => {
    mapRef.current = map;
  }, []);

  const onFileLoaded = useCallback((newPolygons) => {
    const notEmptyPolygons = newPolygons.filter(
      (newPolygon) => newPolygon.paths.length > 0
    );
    myMapContentRef.current.setPolygons((prevValue) => [
      ...prevValue,
      ...notEmptyPolygons,
    ]);
  }, []);

  const onExportClick = () => {
    const rows = toCsv(myMapContentRef.current.polygons);
    let csvContent = "data:text/csv;charset=utf-8,";
    rows.forEach(function(rowArray) {
      let row = rowArray.join(",");
      csvContent += row + "\r\n";
    });
    let linkElement = document.createElement('a');
    linkElement.setAttribute('href', csvContent);
    linkElement.setAttribute('download', "output.csv");
    linkElement.click();
  }

  return (
    <div onContextMenu={(e) => e.preventDefault()}>
      <LoadScript
        className="load-script"
        googleMapsApiKey={process.env.REACT_APP_GOOGLE_MAP_API_KEYS}
        libraries={libraries}
      >
        <GoogleMap
          className="google-map"
          mapContainerStyle={styles.container}
          zoom={10}
          center={center}
          onLoad={onLoad}
        >
          <MyMapContent ref={myMapContentRef}/>
        </GoogleMap>
        <MyCsvReader onFileLoaded={onFileLoaded} />
        <button onClick={onExportClick}>Export CSV</button>
      </LoadScript>
    </div>
  );
};

MyMap.propTypes = MyMapPropTypes;

export default MyMap;
