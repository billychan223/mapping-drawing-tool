import React from "react";
import CSVReader, { IFileInfo } from "react-csv-reader";
import toPolygons from "../utils/toPolygons";

const MyCsvReader = ({ onFileLoaded }: {onFileLoaded: (data: Array<any>, fileInfo?: IFileInfo) => any}) => {
  const handleFile = (data: Array<any> , fileInfo: IFileInfo) => {
    const polygons = toPolygons(data);
    onFileLoaded(polygons);
  };

  const parserOptions = {
    header: true,
    dynamicTyping: true,
    skipEmptyLines: true,
    transformHeader: (header: string) => header.toLowerCase().replace(/\W/g, "_"),
  };
  return (
    <div className="container">
      <CSVReader
        cssClass="react-csv-input"
        label="Import the district CSV"
        onFileLoaded={handleFile}
        parserOptions={parserOptions}
      />
    </div>
  );
};

export default MyCsvReader;
