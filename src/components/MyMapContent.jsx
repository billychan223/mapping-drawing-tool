import { DrawingManager } from "@react-google-maps/api";
import React, { forwardRef, useCallback, useImperativeHandle, useRef, useState } from "react";
import { sortPolygons } from "../utils/toPolygons";
import getIntersect from "../utils/getIntersect";
import MyPolygons from "./MyPolygons";
import PropTypes from "prop-types";
import MyDialog from "./MyDialog";

const drawingManagerOptions = {
    drawingControl: true,
    drawingControlOptions: {
      drawingModes: ["polygon"],
    },
  };

const MyMapContent = forwardRef((props, ref) => {
  const [polygons, setPolygons] = useState([]);
  const myDialogRef = useRef(null);

  const handlePolygonComplete = useCallback((polygon) => {
    const paths = polygon
      .getPath()
      .getArray()
      .map((latLng) => ({ lat: latLng.lat(), lng: latLng.lng() }));
    const newPolygon = {districtid: polygons.length + 1, paths: paths};
    polygon.setMap(null);
    const anyOverlapped = polygons
      .filter((polygon) => polygon.districtid !== newPolygon.districtid)
      .some((polygon) => getIntersect(newPolygon, polygon) !== null);
    if (anyOverlapped) {
      onOverlapDetected();
      return;
    }
    setPolygons((prevValue) => [...prevValue, newPolygon]);
  }, [polygons, setPolygons]);

  useImperativeHandle(ref, () => ({
    setPolygons: setPolygons,
    polygons: polygons
  }));

  const onEditDone = useCallback((newPolygon, oldPolygon) => {
    const anyOverlapped = polygons
    .filter((polygon) => polygon.districtid !== newPolygon.districtid)
    .some((polygon) => getIntersect(newPolygon, polygon) !== null);

    console.log(newPolygon, oldPolygon);

    const polygonNeedToAdd = anyOverlapped ? oldPolygon : newPolygon;
    setPolygons((polygons) => {
      console.log(polygonNeedToAdd.paths);
      polygons = polygons.filter((polygon) => polygon.districtid !== polygonNeedToAdd.districtid);
      polygons.push(polygonNeedToAdd);
      return polygons.sort(sortPolygons);
    });

    if (anyOverlapped) onOverlapDetected();
    
    return anyOverlapped;
  }, [polygons]);

  const onOverlapDetected = () => {
    const myDialog = myDialogRef.current
    if (myDialog !== null) {
      myDialog.openDialog();
    } else {
      console.log("overlap");
    }
  }

  return (
    <div>
      <DrawingManager
        options={drawingManagerOptions}
        onPolygonComplete={handlePolygonComplete}
      />
      <MyPolygons polygons={polygons} onEditDone={onEditDone}/>
      <MyDialog ref={myDialogRef} />
    </div>
  );
});

MyMapContent.displayName = 'MapContent';
MyMapContent.propTypes = {
  onOverlapDetected: PropTypes.func,
};

export default MyMapContent;