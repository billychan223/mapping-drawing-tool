import React from "react";
import MyPolygon from "./MyPolygon";
import PropTypes from "prop-types";

const MyPolygons = (prop) => {
  console.log("rendering mypolygons");
  return (
    <div>
      {prop.polygons.map((polygon) => (
        <MyPolygon
          key={polygon.districtid}
          id={polygon.districtid}
          path={polygon.paths}
          onEditDone={prop.onEditDone}
        />
      ))}
    </div>
  );
};

MyPolygons.propTypes = {
  polygons: PropTypes.array,
  onEditDone: PropTypes.func,
}

export default MyPolygons