import React, { useCallback, useRef, useState } from "react";
import { Polygon } from "@react-google-maps/api";
import PropTypes from "prop-types";

const MyPolygon = (prop) => {
  const [path, setPath] = useState(prop.path);
  const [editable, setEditable] = useState(false);
  const [oldPolygon, setOldPolygon] = useState(null);

  const polygonRef = useRef(null);
  const listenersRef = useRef([]);

  const onEdit = useCallback(() => {
    if (polygonRef.current) {
      const nextPath = polygonRef.current
        .getPath()
        .getArray()
        .map((latLng) => {
          return { lat: latLng.lat(), lng: latLng.lng() };
        });
      setPath(nextPath);
    }
  }, [setPath]);

  const onRightClick = () => {
    if (editable) {
      const newPath = polygonRef.current
        .getPath()
        .getArray()
        .map((latLng) => {
          return { lat: latLng.lat(), lng: latLng.lng() };
        });
        const newPolygon = { districtid: prop.id, paths: newPath };
      const anyOverlapped = prop.onEditDone(newPolygon, oldPolygon);
      setPath(anyOverlapped ? oldPolygon.paths : newPolygon.paths);
    } else {
      setOldPolygon(() => {
        const oldPath = polygonRef.current
          .getPath()
          .getArray()
          .map((latLng) => {
            return { lat: latLng.lat(), lng: latLng.lng() };
          });
        const oldPolygon = { districtid: prop.id, paths: oldPath };
        return oldPolygon;
      })
    }
    setEditable(!editable);
  };

  const onLoad = useCallback(
    (polygon) => {
      polygonRef.current = polygon;
      const path = polygon.getPath();
      listenersRef.current.push(
        path.addListener("set_at", onEdit),
        path.addListener("insert_at", onEdit),
        path.addListener("remove_at", onEdit)
      );
    },
    [onEdit]
  );

  const onUnmount = useCallback(() => {
    listenersRef.current.forEach((lis) => lis.remove());
    polygonRef.current = null;
  }, []);

  return (
    <Polygon
      paths={path}
      options={{
        clickable: true,
        draggable: editable,
        editable: editable,
      }}
      onRightClick={onRightClick}
      onLoad={onLoad}
      onUnmount={onUnmount}
    />
  );
};

MyPolygon.propTypes = {
  id: PropTypes.number,
  path: PropTypes.array,
};

export default MyPolygon;
