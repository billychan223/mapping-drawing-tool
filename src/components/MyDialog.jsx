import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from "@material-ui/core";
import React, { forwardRef, useImperativeHandle, useState } from "react";

const MyDialog = forwardRef((_props, ref) => {
    const [open, setOpen] = useState(false);

    const openDialog = () => setOpen(true);

    const closeDialog = () => setOpen(false);

    useImperativeHandle(ref, () => ({
        openDialog: openDialog,
        closeDialog: closeDialog
      }));

    return (<Dialog
        open={open}
        onClose={closeDialog}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          WARNING
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Overlap detected. Please be careful!
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={closeDialog} color="primary" autoFocus>
            OK
          </Button>
        </DialogActions>
      </Dialog>)
});
MyDialog.displayName = 'MyDialog';

export default MyDialog;